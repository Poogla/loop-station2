	<?php 
class M_siswa extends CI_Model{

	function get_all_siswa(){
		$hsl=$this->db->query("SELECT tbl_siswa.*,kelas_nama FROM tbl_siswa JOIN tbl_kelas ON siswa_kelas_id=kelas_id");
		return $hsl;
	}

	function get_all_guru(){
		$hsl=$this->db->query("SELECT tbl_siswa.*,guru_nama FROM tbl_siswa JOIN tbl_guru ON loop_buddy_id=guru_id");
		return $hsl;
	}

	function simpan_siswa($nis,$nama,$kontak,$jumlahanggota,$photo,$loopbud){
		$hsl=$this->db->query("INSERT INTO tbl_siswa (siswa_nis,siswa_nama,kontak,jumlah_anggota,loop_buddy_id,siswa_photo) VALUES ('$nis','$nama','$kontak','$jumlahanggota','$loopbud','$photo')");
		return $hsl;
	}
	function simpan_siswa_tanpa_img($nis,$nama,$kontak,$jumlahanggota,$loopbud){
		$hsl=$this->db->query("INSERT INTO tbl_siswa (siswa_nis,siswa_nama,kontak,jumlah_anggota,loop_buddy_id) VALUES ('$nis','$nama','$kontak','$jumlahanggota','$loopbud')");
		
		return $hsl;
	}

	function update_siswa($kode,$nis,$nama,$kontak,$jumlahanggota,$photo,$loopbud){
		$hsl=$this->db->query("UPDATE tbl_siswa SET siswa_nis='$nis',siswa_nama='$nama',kontak='$kontak',jumlah_anggota='$jumlahanggota',siswa_photo='$photo',loop_buddy_id='$loopbud' WHERE siswa_id='$kode'");
		return $hsl;
	}
	function update_siswa_tanpa_img($kode,$nis,$nama,$kontak,$jumlahanggota,$loopbud){
		$hsl=$this->db->query("UPDATE tbl_siswa SET siswa_nis='$nis',siswa_nama='$nama',kontak='$kontak',jumlah_anggota='$jumlahanggota',loop_buddy_id='$loopbud' WHERE siswa_id='$kode'");
		return $hsl;
	}
	function hapus_siswa($kode){
		$hsl=$this->db->query("DELETE FROM tbl_siswa WHERE siswa_id='$kode'");
		return $hsl;
	}

	function siswa(){
		$hsl=$this->db->query("SELECT tbl_siswa.*,kelas_nama FROM tbl_siswa JOIN tbl_kelas ON siswa_kelas_id=kelas_id");
		return $hsl;
	}

	function guru(){
		$hsl=$this->db->query("SELECT tbl_siswa.*,guru_nama FROM tbl_siswa JOIN tbl_guru ON loop_buddy_id=guru_id");
		return $hsl;
	}
	function siswa_perpage($offset,$limit){
		$hsl=$this->db->query("SELECT tbl_siswa.*,kelas_nama FROM tbl_siswa JOIN tbl_kelas ON siswa_kelas_id=kelas_id limit $offset,$limit");
		return $hsl;
	}

}